# Copyright (C) 2019, 2020, 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libsg3d.a
LIBNAME_SHARED = libsg3d.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC =\
 src/sg3d_device.c\
 src/sg3d_geometry.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then \
	     echo "$(LIBNAME)"; \
	   else \
	     echo "$(LIBNAME_SHARED)"; \
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(RSYS_LIBS) -lm

$(LIBNAME_STATIC): libsg3d.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libsg3d.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) $(CFLAGS_SO) $(RSYS_CFLAGS) -DSG3D_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    sg3d.pc.in > sg3d.pc

sg3d-local.pc: sg3d.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    sg3d.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" sg3d.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sg3d.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sg3d_sXd_helper.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sg3d_sencXd_helper.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sg3d_sdisXd_helper.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sgX3d.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/sgX3d_undefs.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-geometry-3d" \
	COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/sg3d.pc"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-geometry-3d/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-geometry-3d/README.md"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sg3d.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sg3d_sXd_helper.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sg3d_sencXd_helper.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sg3d_sdisXd_helper.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sgX3d.h"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/sgX3d_undefs.h"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(TEST_OBJ_S3DUT) $(LIBNAME)
	rm -f .config .config_test .test libsg3d.o sg3d.pc sg3d-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP) $(TEST_DEP_S3DUT)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_sg3d_cube_behind_cube.c\
 src/test_sg3d_cube_in_cube.c\
 src/test_sg3d_cube_on_cube.c\
 src/test_sg3d_device.c\
 src/test_sg3d_geometry_2.c\
 src/test_sg3d_geometry.c\
 src/test_sg3d_invalid_models.c\
 src/test_sg3d_multi_media.c\
 src/test_sg3d_unspecified_properties.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SG3D_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags sg3d-local.pc)
SG3D_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs sg3d-local.pc)

# Regular Compiler and linker flags
TEST_CFLAGS = $(CFLAGS_EXE) $(SG3D_CFLAGS) $(RSYS_CFLAGS)
TEST_LIBS = $(LDFLAGS_EXE) $(SG3D_LIBS) $(RSYS_LIBS)

build_tests: .config_test build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) \
	test_bin

.config_test: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(S3DUT_VERSION) s3dut; then \
	  echo "s3dut $(S3DUT_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	@$(SHELL) make.sh clean_test $(TEST_SRC)

################################################################################
# Regular tests
################################################################################
src/test_sg3d_cube_behind_cube.d \
src/test_sg3d_cube_in_cube.d \
src/test_sg3d_cube_on_cube.d \
src/test_sg3d_device.d \
src/test_sg3d_geometry_2.d \
src/test_sg3d_geometry.d \
src/test_sg3d_invalid_models.d \
src/test_sg3d_multi_media.d \
src/test_sg3d_unspecified_properties.d \
: config.mk sg3d-local.pc
	@$(CC) $(TEST_CFLAGS) -MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/test_sg3d_cube_behind_cube.o \
src/test_sg3d_cube_in_cube.o \
src/test_sg3d_cube_on_cube.o \
src/test_sg3d_device.o \
src/test_sg3d_geometry_2.o \
src/test_sg3d_geometry.o \
src/test_sg3d_invalid_models.o \
src/test_sg3d_multi_media.o \
src/test_sg3d_unspecified_properties.o \
: config.mk sg3d-local.pc
	$(CC) $(TEST_CFLAGS) -c $(@:.o=.c) -o $@

test_sg3d_cube_behind_cube \
test_sg3d_cube_in_cube \
test_sg3d_cube_on_cube \
test_sg3d_device \
test_sg3d_geometry_2 \
test_sg3d_geometry \
test_sg3d_invalid_models \
test_sg3d_multi_media \
test_sg3d_unspecified_properties \
: config.mk sg3d-local.pc $(LIBNAME)
	$(CC) $(TEST_CFLAGS) -o $@ src/$@.o $(TEST_LIBS)

################################################################################
# Test based on Star-3DUT
################################################################################
src/test_sg3d_many_enclosures.d \
src/test_sg3d_many_triangles.d \
src/test_sg3d_some_enclosures.d \
src/test_sg3d_some_triangles.d \
: config.mk sg3d-local.pc
	@$(CC) $(TEST_CFLAGS) $(S3DUT_CFLAGS) -MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

src/test_sg3d_many_enclosures.o \
src/test_sg3d_many_triangles.o \
src/test_sg3d_some_enclosures.o \
src/test_sg3d_some_triangles.o \
: config.mk sg3d-local.pc
	$(CC) $(TEST_CFLAGS) $(S3DUT_CFLAGS) -c $(@:.o=.c) -o $@

test_sg3d_many_enclosures \
test_sg3d_many_triangles \
test_sg3d_some_enclosures \
test_sg3d_some_triangles \
: config.mk sg3d-local.pc $(LIBNAME)
	$(CC) $(TEST_CFLAGS) $(S3DUT_CFLAGS) -o $@ src/$@.o $(TEST_LIBS) $(S3DUT_LIBS)
